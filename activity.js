/*

- What directive is used by Node.js in loading the modules it needs?
- What Node.js module contains a method for server creation?
- What is the method of the http object responsible for creating a server using Node.js?
- What method of the response object allows us to set status codes and content types?
- Where will console.log() output its contents when run in Node.js?
- What property of the request object contains the address's endpoint?

*/

//1. What directive is used by Node.js in loading the modules it needs?

require()function

//2. What Node.js module contains a method for server creation?
http module

//3. What is the method of the http object responsible for creating a server using Node.js?
createServer()

//4. What method of the response object allows us to set status codes and content types?
response.writeHead(status code, {content type})

//5. Where will console.log() output its contents when run in Node.js?
terminal (gitbash)

//6. What property of the request object contains the address's endpoint?
web address/URL